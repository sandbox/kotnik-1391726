                             Introduction

Freightquote.com and  their subsidiaries serve  customers across North
America  (US  and  Canada).   Commerce  Freightquote  module  provides
realtime shipping cost estimates for Freightquote service.

This   module  is   in  the   Beta  state   and  it   imposes  certain
limitations. You can find those limitations further down in this file.

                             Dependencies

This module depends on following modules:

Commerce - drupal.org/project/commerce -  Commerce module that must be
installed and  set up before  being able to use  Commerce Freightquote
module.

Commerce  Physical -  drupal.org/project/commerce_physical  - Used  to
define  physical properties of  the goods  estimated for  delivery via
Frieghtquote.  This module depends on Physical Fields module that must
be installed as well (drupal.org/project/physical).

Commerce    Shipping    -    drupal.org/project/commerce_shipping    -
Infrastructure needed for integration with the Commerce module.

                             Instalation

Install and enable the module  and all its dependencies. Configure all
shippable  product  types and  add  dimensions  and weight.  Configure
'Shipping service'  checkout pane and Freightquote  settings. You must
have Freightquote.com account.

                   Authors/Maintainers/Contributors

Nikola Kotur (http://drupal.org/user/16132)
