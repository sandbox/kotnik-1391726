<?php

/**
 * @file
 * Handles admin settings page for Commerce Freightquote module.
 */

/**
 * Implements hook_settings_form().
 */
function commerce_freightquote_settings_form($form, &$form_state) {
  // API information
  $fc_customerid = variable_get('commerce_freightquote_customer_id', '');
  $fc_userid     = variable_get('commerce_freightquote_user_id', '');
  $form['api'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Freightquote API credentials'),
    '#collapsible' => TRUE,
    '#description' => t('In order to obtain shipping rate estimates, you must have an API account with Freightquote. You can apply for Freightquote API credentials at '. l('Freightquote.com', 'http://www.freightquote.com/contact/', array('attributes' => array('target'=>'_blank')))),
  );
  $form['api']['commerce_freightquote_customer_id'] = array(
    '#type'          => "textfield",
    '#title'         => t("Customer ID"),
    '#default_value' => $fc_customerid,
    '#required'      => TRUE,
  );
  $form['api']['commerce_freightquote_user_id'] = array(
    '#type'          => "textfield",
    '#title'         => t("Access Code"),
    '#default_value' => $fc_userid,
    '#required'      => TRUE,
    '#description'   => t('Also refered as Log In, or E-mail address.'),
  );
  $form['api']['commerce_freightquote_password'] = array(
    '#type'        => "password",
    '#title'       => t("Password"),
    '#required'    => !variable_get('commerce_freightquote_password', FALSE),
    '#description' => t('Please leave blank if you do not want to update your password at this time.'),
  );

  // Origin information
  $fc_company = variable_get('commerce_freightquote_company_name', '');
  $fc_arrival = variable_get('commerce_freightquote_arrival_info', 'false');
  $fc_dock = variable_get('commerce_freightquote_dock', 'false');
  $fc_construction = variable_get('commerce_freightquote_construction', 'false');
  $fc_inside = variable_get('commerce_freightquote_inside', 'false');
  $fc_trade = variable_get('commerce_freightquote_trade', 'false');
  $fc_residential = variable_get('commerce_freightquote_residential', 'false');
  $fc_liftgate = variable_get('commerce_freightquote_liftgate', 'false');
  $form['origin'] = array(
    '#type' => "fieldset",
    '#title' => t("Shipping origin"),
    '#collapsible' => true
  );
  $form['origin']['commerce_freightquote_company_name'] = array(
    '#type' => "textfield",
    '#title' => t("Company Name"),
    '#default_value' => $fc_company,
    '#value' => variable_get('commerce_freightquote_company_name', '')
  );
  $form['origin']['commerce_freightquote_arrival_info'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_arrival,
    '#title' => t('Requires Arrival Information'),
  );
  $form['origin']['commerce_freightquote_dock'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_dock,
    '#title' => t('Has Loading Dock'),
  );
  $form['origin']['commerce_freightquote_construction'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_construction,
    '#title' => t('Is Construction Site'),
  );
  $form['origin']['commerce_freightquote_inside'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_inside,
    '#title' => t('Requires Inside Delivery'),
  );
  $form['origin']['commerce_freightquote_trade'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_trade,
    '#title' => t('Trade Show'),
  );
  $form['origin']['commerce_freightquote_residential'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_residential,
    '#title' => t('Residential'),
  );
  $form['origin']['commerce_freightquote_liftgate'] = array(
    '#type' => 'radios',
    '#options' => array('true' => t('Yes'), 'false' => t('No')),
    '#default_value' => $fc_liftgate,
    '#title' => t('Requires Liftgate'),
  );
  $form['origin']['commerce_freightquote_postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_freightquote_postcode', ''),
    '#size' => 6,
  );
  $form['origin']['commerce_freightquote_country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_freightquote_country', ''),
    '#options' => array(
      '' => t('Please Select'),
      'US' => t('USA'),
      'CA' => t('Canada'),
    )
  );

  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t("Enable Freightquote Shipping Service "),
    '#collapsible' => TRUE
  );
  $form['services']['commerce_freightquote_services'] = array(
    '#type' => 'checkboxes',
    '#options' => array('freightquote_shipping' => 'Freightquote shipping'),
    '#default_value' => variable_get("commerce_freightquote_services", array())
  );

  $form['commerce_freightquote_show_logo'] = array(
    "#type" => "checkbox",
    "#title" => t("Show Freightquote Logo on Shipping Page"),
    "#default_value" => variable_get("commerce_freightquote_show_logo", 0),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/* hook_settings_form_validate() */
function commerce_freightquote_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  // If the Password field is empty, then they're not trying to update it and we should ignore it.
  if (empty($values['commerce_freightquote_password'])) {
    unset($form_state['values']['commerce_freightquote_password']);
    return;
  }
}

/**
 * Implements hook_form_submit().
 */
function commerce_freightquote_settings_form_submit($form, &$form_state) {
  if (empty($form_state['input']['commerce_freightquote_password'])) {
    unset($form_state['input']['commerce_freightquote_password']);
  }

  $fields = array(
    'commerce_freightquote_customer_id',
    'commerce_freightquote_user_id',
    'commerce_freightquote_password',
    'commerce_freightquote_company_name',
    'commerce_freightquote_arrival_info',
    'commerce_freightquote_dock',
    'commerce_freightquote_construction',
    'commerce_freightquote_inside',
    'commerce_freightquote_trade',
    'commerce_freightquote_residential',
    'commerce_freightquote_liftgate',
    'commerce_freightquote_postcode',
    'commerce_freightquote_country',
    'commerce_freightquote_services',
    'commerce_freightquote_show_logo',
  );

  foreach ($fields as $key) {
    if (array_key_exists($key, $form_state['input'])) {
      $value = $form_state['input'][$key];
      variable_set($key, $value);
    }
  }

  drupal_set_message(t('Freightquote configuration has been saved.'));
}
