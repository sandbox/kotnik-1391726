<?php

// $Id$

/**
 * @file
 * Handles API calls for Commerce Freightquote module.
 */

/*
 * Generating XML for API request.
 */
function commerce_freightquote_build_rate_request($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Determine the shipping profile reference field name for the order.
  $field_name = commerce_physical_order_shipping_field_name($order);
  $shipping_profile = $order_wrapper->{$field_name}->value();

  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->{$field_name}->commerce_customer_address)) {
    $shipping_address = $order_wrapper->{$field_name}->commerce_customer_address->value();
  }
  else {
    $shipping_address = addressfield_default_values();
  }

  $weight = commerce_physical_order_weight($order, 'lb');  // this returns $weight['unit'] and $weight['weight']

  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->commerce_customer_shipping->commerce_customer_address)) {
    $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
  }

  // Generate XML for request.
  $xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <soap:Body>
    <GetRatingEngineQuote xmlns="http://tempuri.org/" />
  </soap:Body>
</soap:Envelope>
');

  // Get GetRatingEngineQuote node.
  $xml_body = $xml->children('http://schemas.xmlsoap.org/soap/envelope/');
  $xml_greq = $xml_body->children();

  $xml_req = $xml_greq->addChild('request');
  $xml_user = $xml_greq->addChild('user');

  // Add authenticiation info.
  $xml_user->addChild('Name', variable_get('commerce_freightquote_user_id', ''));
  $xml_user->addChild('Password', variable_get('commerce_freightquote_password', ''));

  // Create quote request.
  $xml_req->addChild('CustomerId', variable_get('commerce_freightquote_customer_id', ''));
  $xml_req->addChild('QuoteType', 'B2B');
  $xml_req->addChild('ServiceType', 'LTL');
  $xml_req_qs = $xml_req->addChild('QuoteShipment');

  $xml_locs = $xml_req_qs->addChild('ShipmentLocations');
  $xml_prods = $xml_req_qs->addChild('ShipmentProducts');
  $xml_contacts = $xml_req_qs->addChild('ShipmentContacts');

  // Add origin.
  $xml_loc = $xml_locs->addChild('Location');
  $xml_loc->addChild('LocationType', 'Origin');
  $xml_loc->addChild('RequiresArrivalNotification', variable_get('commerce_freightquote_arrival_info', 'false'));
  $xml_loc->addChild('HasLoadingDock', variable_get('commerce_freightquote_dock', 'false'));
  $xml_loc->addChild('IsConstructionSite', variable_get('commerce_freightquote_construction', 'false'));
  $xml_loc->addChild('RequiresInsideDelivery', variable_get('commerce_freightquote_inside', 'false'));
  $xml_loc->addChild('IsTradeShow', variable_get('commerce_freightquote_trade', 'false'));
  $xml_loc->addChild('IsResidential', variable_get('commerce_freightquote_residential', 'false'));
  $xml_loc->addChild('RequiresLiftgate', variable_get('commerce_freightquote_liftgate', 'false'));
  $xml_loc_addr = $xml_loc->addChild('LocationAddress');
  $xml_loc_addr->addChild('PostalCode', variable_get('commerce_freightquote_postcode', ''));
  $xml_loc_addr->addChild('CountryCode', variable_get('commerce_freightquote_country', 'US'));
  // Add destination.
  $xml_loc = $xml_locs->addChild('Location');
  $xml_loc->addChild('LocationType', 'Destination');
  $xml_loc->addChild('RequiresArrivalNotification', 'false');
  $xml_loc->addChild('HasLoadingDock', 'false');
  $xml_loc->addChild('IsConstructionSite', 'false');
  $xml_loc->addChild('RequiresInsideDelivery', 'false');
  $xml_loc->addChild('IsTradeShow', 'false');
  $xml_loc->addChild('IsResidential', 'false');
  $xml_loc->addChild('RequiresLiftgate', 'false');
  $xml_loc_addr = $xml_loc->addChild('LocationAddress');
  $xml_loc_addr->addChild('PostalCode', '30303');
  $xml_loc_addr->addChild('CountryCode', 'US');

  // Add products
  $xml_prod = $xml_prods->addChild('Product');
  $xml_prod->addChild('Class', '55');
  $xml_prod->addChild('Weight', '1200');
  $xml_prod->addChild('Length', '0');
  $xml_prod->addChild('Width', '0');
  $xml_prod->addChild('Height', '0');
  $xml_prod->addChild('ProductDescription', 'Books');
  $xml_prod->addChild('PackageType', 'Pallets_48x48');
  $xml_prod->addChild('IsStackable', 'false');
  $xml_prod->addChild('DeclaredValue', '0');
  $xml_prod->addChild('CommodityType', 'GeneralMerchandise');
  $xml_prod->addChild('ContentType', 'NewCommercialGoods');
  $xml_prod->addChild('IsHazardousMaterial', 'false');
  $xml_prod->addChild('PieceCount', '5');
  $xml_prod->addChild('ItemNumber', '0');

  return $xml->asXML();
}

/**
 * Submits an API request to the Progistics XML Processor.
 *
 * @param $xml
 *   An XML string to submit to the Progistics XML Processor.
 * @param $message
 *   Optional log message to use for logged API requests.
 */
function commerce_freightquote_api_request($xml, $message = '') {
  // Log the API request if specified.
  if (in_array('request', variable_get('commerce_freightquote_log', array()))) {
    if (empty($message)) {
      $message = t('Submitting API request to the Freightquote');
    }
    watchdog('freightquote', '@message:<pre>@xml</pre>', array('@message' => $message, '@xml' => $xml));
  }

  $ch = curl_init('https://b2b.Freightquote.com/WebService/QuoteService.asmx');
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 60);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
  $result = curl_exec($ch);

  // Log any errors to the watchdog.
  if ($error = curl_error($ch)) {
    watchdog('freightquote', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    return FALSE;
  }
  curl_close($ch);

  // If we received data back from the server...
  if (!empty($result)) {
    // Extract the result into an XML response object.
    $response = new SimpleXMLElement($result);

    // Log the API request if specified.
    if (in_array('response', variable_get('commerce_freightquote_log', array()))) {
      watchdog('ups', 'API response received:<pre>@xml</pre>', array('@xml' => $response->asXML()));
    }
    return $response;
  }
  else {
    return FALSE;
  }
}
